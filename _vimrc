execute pathogen#infect()
syntax on
set backspace=2
filetype plugin indent on
set clipboard=unnamed
set sw=4 expandtab smarttab autoindent
set directory=.,$TEMP
set gfn=Consolas:h12:cANSI
set hlsearch
set enc=utf-8

colorscheme desert

set laststatus=2

let g:gitgutter_sign_column_always = 1
"let g:gitgutter_sign_added = 'xx'
"let g:gitgutter_sign_modified = 'yy'
"let g:gitgutter_sign_removed = 'zz'
"let g:gitgutter_sign_removed_first_line = '^^'
"let g:gitgutter_sign_modified_removed = 'ww'

let g:gitgutter_enabled = 1
let g:gitgutter_signs = 1
let g:gitgutter_highlight_lines = 0
let g:gitgutter_async = 1

let g:gitgutter_grep_command = 'grep -e'

let g:tsuquyomi_use_vimproc = 1

set number
hi LineNr guifg=grey40
set updatetime=250
"set guioptions=grLtmTe
set guioptions=gtre

if has("gui_running")
  if has("gui_gtk2") || has("gui_gtk3")
    set guifont=Inconsolata\ for\ Powerline\ 12
  elseif has("gui_macvim")
    set guifont=Menlo\ Regular:h14
  elseif has("gui_win32")
    set guifont=Anonymice\ Powerline:h12
  endif
endif
let guifontpp_original_font_map="<C-F10>"

" air-line
"let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_theme='minimalist'
let g:bufferline_echo = 0

" syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" swap between header and source
nnoremap <M-o> :e %:p:s,.h$,.X123X,:s,.cpp$,.h,:s,.X123X$,.cpp,<CR>
let g:syntastic_enable_signs = 1

" flake8
"autocmd BufWritePost *.py call flake8#Flake8()
"let g:flake8_show_in_file=1
"let g:flake8_show_in_gutter=1
"let g:flake8_show_quickfix=1
