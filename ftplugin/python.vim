setlocal sw=4
setlocal smarttab
setlocal expandtab
setlocal cindent
setlocal number

set makeprg=python\ %
setlocal errorformat=%C\ %.%#,%A\ \ File\ \"%f\"\\,\ line\ %l%.%#,%Z%[%^\ ]%\\@=%m
nnoremap <leader>m :w <cr> :silent make<bar>
            \:if v:shell_error<bar>
            \  copen<bar>
            \endif<cr>

"inoremap # #
